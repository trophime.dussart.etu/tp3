export default class Router {
	static titleElement;
	static contentElement;
	static routes = [];

	static #menuElement;
	static set menuElement(element) {
        this.#menuElement = element;
        const link = document.querySelectorAll('header a');
        for (let i = 0; i < link.length; i++) {
            link[i].addEventListener('click', e => {
                e.preventDefault();
                console.log(link[i].getAttribute('href'));
                Router.navigate(link[i].getAttribute('href'));
            });
        }
    }

	static navigate(path) {
		const route = this.routes.find(route => route.path === path);
		if (route) {
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			this.contentElement.innerHTML = route.page.render();
			route.page.mount?.(this.contentElement);
		}
	}
}
