import Page from './Page.js';

export default class PizzaForm extends Page {
    render() {
        return /*html*/ `
            <form class="pizzaForm">
                <label>
                    Nom :
                    <input type="text" name="name">
                </label>
                <button type="submit">Ajouter</button>
            </form>`;
    }

    mount(element) {
        super.mount(element);
        const form = document.querySelector('form'),
            input = form.querySelector(`input[name=name]`);

        form.addEventListener('submit', e => {
            e.preventDefault();
            this.submit(e);
            console.log(input.value);
        });
    }

    submit(event) {
        const form = document.querySelector('form'),
            input = form.querySelector(`input[name=name]`);
        if (input.value === '') {
            alert(`Erreur : pas de nom`);
        } else {
            alert(`La pizza ${input.value} a été ajoutée`);
        }
    }
}